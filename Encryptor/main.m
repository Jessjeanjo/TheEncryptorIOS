//
//  main.m
//  Encryptor
//
//  Created by Jessica Joseph on 12/8/15.
//  Copyright © 2015 Jessica Joseph. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
