//
//  AppDelegate.h
//  Encryptor
//
//  Created by Jessica Joseph on 12/8/15.
//  Copyright © 2015 Jessica Joseph. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

